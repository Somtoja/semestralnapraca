<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <title>VAII / Blog</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="../ext/style.css">
</head>
<body class="edit">
<div class="posts-div">

    <?php
    // Create connection
    $mysqli = new mysqli("localhost:3308", "Rado", "adminer", "rado-vaii");
    $query = "SELECT * FROM posts";

    if(count($_POST)>0) {
        mysqli_query($mysqli,"UPDATE posts set id_post='" . $_POST['id_post'] . "', nadpis'" . $_POST['nadpis'] . "', text='" . $_POST['text'] . "' WHERE id_post='" . $_POST['id_post'] . "'");

        $message = "Record Modified Successfully";
        $result = mysqli_query($mysqli,"SELECT * FROM posts WHERE id_post='" . $_GET['id_post'] . "'");
        $row= mysqli_fetch_array($result);

    }
    ?>

     <h2 class="post-title">Editovat prispevok</h2>
        <div class="post-text">
            <form class="post-form" method="POST" action="update-post.php">
                <label for="id_post">
                    ID
                    <input type="text" id="id_post" name="id_post" required>
                </label>
                <label for="nadpis">
                    Nadpis
                    <input type="text" id="nadpis" name="nadpis" required>
                </label>
                <label for="text">
                    Text
                <textarea id="text" name="text" cols="40" rows="10" required></textarea>
                 </label>
                <div class="btn" id="post-submit-btn" >Editovať</div>
            </form>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="../ext/app.js"></script>

</body>
</html>
