<!DOCTYPE html>
<html lang="sk">
<head>
	<meta charset="utf-8">
	<title>VAII / Index</title>
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
	<link rel="stylesheet" href="ext/style.css">
</head>
<body class="home">

<?php
require 'header.php';
?>

	<main>
		<article class="content container">
			<h1 class="shadow">VAII<br>Semestrálna<br>práca</h1>
            <?php
            if (isset($_SESSION['username'])) {
                echo "<h2 class=\"shadow\">
			Vitaj, 
			</h2>";
            }
            ?>
			<h2 class="shadow">
			Moja semestrálna práca na predmet Vývoj aplikácii pre internet a intranet.
			</h2>

			<a href="#index-image" class="btn btn-red">SCROLL DOWN</a>

			<p class="small">
				V tejto semestrálnej práci sa nachádza Hlavná stránka, Galéria, Kontakt na mňa a Blog,
				do ktorého môžem pridávať príspevky.
			</p>

			<img src="img/8.png" class="index-image" id="index-image" alt="">
		</article>
	</main>

	<aside class="pre-footer">
		<div class="container">
			<h3>Footer</h3>

			<ul>
				<li class="green"><a href="index.php">Index</a></li>
				<li class="yellow"><a href="galeria.php">Galéria</a></li>
				<li class="red"><a href="kontakt.php">Kontakt</a></li>
				<li class="blue"><a href="blog.php">Blog</a></li>
			</ul>

			<p>Predmet : 5US109 vývoj aplikácií pre internet a intranet<br>
				Radovan Žiak<br>
				5ZY038</p>

			<a href="#" class="btn btn-green">Scroll up</a>
		</div>
	</aside>

<?php
require 'footer.php';
?>

</body>
</html>
