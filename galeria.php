<!DOCTYPE html>
<html lang="sk">
<head>
	<meta charset="utf-8">
	<title>VAII / Galéria</title>
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
	<link rel="stylesheet" href="ext/style.css">
    <!-- comments <link rel="stylesheet" href="ext/fancybox/jquery.fancybox-1.3.4.css"/>  -->
</head>
<body class="gallery">

<?php
require 'header.php';
?>

	<main>
		<section class="content container">
			<h1 class="shadow">Galéria</h1>
			<h2 class="shadow">Tu sa nachádzajú obrázky z môjho obľúbeného športu.</h2>

		 	<div class="image-grid group">
				<a class="singleimage" href="img/2.png"><img src="img/2.png" class="gallery-img" alt=""></a>
				<a class="singleimage" href="img/5.png"><img src="img/5.png" class="gallery-img" alt=""></a>
				<a class="singleimage" href="img/7.png"><img src="img/7.png" class="gallery-img" alt=""></a>
				<a class="singleimage" href="img/6.png"><img src="img/6.png" class="gallery-img" alt=""></a>
				<a class="singleimage" href="img/4.png"><img src="img/4.png" class="gallery-img" alt=""></a>
				<a class="singleimage" href="img/3.png"><img src="img/3.png" class="gallery-img" alt=""></a>
			</div>
		</section>
	</main>

	<aside class="pre-footer">
		<div class="container">
			<h3>Footer</h3>

			<ul>
				<li class="green"><a href="index.php">Index</a></li>
				<li class="yellow"><a href="galeria.php">Galéria</a></li>
				<li class="red"><a href="kontakt.php">Kontakt</a></li>
				<li class="blue"><a href="blog.php">Blog</a></li>
			</ul>

			<p>Predmet : 5US109 vývoj aplikácií pre internet a intranet<br>
				Radovan Žiak<br>
				5ZY038</p>

			<a href="#" class="btn btn-green">Scroll up</a>
		</div>
	</aside>

<?php
require 'footer.php';
?>


<!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js"></script> -->
<!-- <script type="text/javascript" src="ext/fancybox/jquery.fancybox-1.3.4.pack.js"></script>-->
<!--<script type="text/javascript" src="ext/fancybox/jquery.easing-1.3.pack.js"></script>-->
<!--<script src="ext/app.js"></script>-->


</body>
</html>
