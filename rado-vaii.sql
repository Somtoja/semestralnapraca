-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `rado-vaii`;

CREATE TABLE `emails` (
  `meno` text NOT NULL,
  `email` text NOT NULL,
  `sprava` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `posts` (
  `nadpis` text NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `posts` (`nadpis`, `text`) VALUES
('Cotton candy',	'Donut sweet macaroon sweet roll I love sweet roll tart. Brownie marzipan cheesecake carrot cake tiramisu marzipan pudding. Cupcake marzipan fruitcake chocolate bar bear claw. Lemon drops candy pastry powder. Gingerbread pastry macaroon. Cotton candy bonbon candy chupa chups cupcake croissant.'),
('Marzipan macaroon',	'Marzipan macaroon sweet I love cotton candy. Ice cream marshmallow I love topping topping tiramisu halvah sweet. Cake halvah brownie pastry topping. Cheesecake oat cake carrot cake tiramisu marzipan gummi bears cookie topping bonbon. Jelly-o jelly-o fruitcake. Danish topping gingerbread candy canes bonbon. I love bear claw soufflé sugar plum jelly beans cotton candy I love I love.'),
('Candy canes',	'Candy canes carrot cake candy apple pie jujubes pie I love cake pastry. Apple pie gingerbread fruitcake jujubes gummies candy liquorice wafer. Gummies I love pastry danish cupcake halvah chupa chups sesame snaps. '),
('Bonbon gummies',	'Bonbon gummies I love cake I love toffee halvah I love marzipan. Marshmallow cheesecake dessert sweet brownie. I love apple pie carrot cake. Jelly beans ice cream donut sugar plum topping I love. Icing dessert tiramisu. Topping candy brownie tootsie roll topping I love topping sesame snaps.'),
('Pastry sweet rolls',	'Pastry I love pudding sweet roll. Muffin chocolate caramels sweet candy canes tart apple pie sweet. Gingerbread chocolate apple pie. Biscuit jujubes I love bear claw toffee I love I love dessert. Liquorice lollipop carrot cake. Apple pie ice cream jelly-o.'),
('Sesame deserts',	'Donut brownie powder pie cotton candy I love I love. I love apple pie tiramisu topping dessert chocolate cake cake sesame snaps sweet roll. Topping croissant biscuit tootsie roll I love. Cupcake jelly-o sesame snaps dessert.'),
('Jelly ice cream',	'I love chocolate cake dessert sugar plum lollipop I love donut. Pie pie I love ice cream I love. Jelly ice cream cake ice cream dragée macaroon dragée.'),
('Drage chocolate',	'Drage chocolate bonbon. Brownie topping I love caramels I love. Jelly-o sesame snaps candy jelly beans tootsie roll lemon drops. Caramels jelly-o I love liquorice tootsie roll apple pie.');

-- 2018-11-11 23:07:13
