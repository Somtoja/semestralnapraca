
$(document).ready(function() {

        // contact page
        $('#meno').focus();
        var form = $('.contact-form');

        $(form).submit(function (event) {
            event.preventDefault();

            var meno = $('#meno').val();
            var email = $('#email').val();
            var sprava = $('#sprava').val();

            var ajax = $.ajax({
                context: this,
                url: form.attr('action'),
                type: 'POST',
                data: {meno: meno, email: email, sprava: sprava},
                dataType: 'text',
                success: function(){},
                error: function(){ console.log('error'); }
            });

            alert('Správa odoslaná !');
            $('#meno').val('');
            $('#email').val('');
            $('#sprava').val('');
        });

        // blog page
        $('.posts-div').find('.post').first().show();
        $('.post-preview').first().addClass('active');

        $('.post-preview').click(function () {
            $(this).addClass('active').siblings().removeClass('active');
            var index = $(this).index();
            var post = $('.posts-div').find('.post')[index];
            $(post).siblings().hide();
            $(post).fadeIn();
        });

        $('.trash-icon').parent().click(function (event) {
            event.preventDefault();
            var nadpis = $(this).parent().find('.post-title').text();

            $(this).parent().hide().next().fadeIn();
            var actual = $('.post-preview.active');
            var index = actual.index();
            actual.next().addClass('active');
            actual.remove();
            $('.post')[index].remove();

            var ajax = $.ajax({
                context: this,
                url: './phps/delete-post.php',
                type: 'POST',
                data: {nadpis: nadpis},
                dataType: 'text',
                success: function(){},
                error: function(){ console.log('error'); }
            });
        });

        $('.btn-add-post').click(function () {
            $('.post').hide();
            $('.add-post-form').fadeIn();
        });

        $('#post-submit-btn').click(function (event) {
            event.preventDefault();

            var nadpis = $('#nadpis').val();
            var text = $('#text').val();

            var ajax = $.ajax({
                context: this,
                url: $('.post-form').attr('action'),
                type: 'POST',
                data: {nadpis: nadpis, text: text},
                dataType: 'text',
                success: function(){},
                error: function(){ console.log('error'); }
            });

            $('.sidebar').append(
                '<div class="post-preview">' +
                '<p>' + nadpis + '</p>' +
                '</div>'
            );

            $('.posts-div').append(
                '<article class="post" style="display: none;">' +
                '<h1 class="post-title shadow">' + nadpis + '</h1>' +
                '<div class="post-text shadow">' +
                '<p>' + text + '</p>' +
                '</div>' +
                '<a href=""><i class="trash-icon fas fa-2x fa-trash-alt"></i></a>' +
                '</article>'
            );

            $('.post-preview').last().click(function () {
                $(this).addClass('active').siblings().removeClass('active');
                var index = $(this).index();
                var post = $('.posts-div').find('.post')[index];
                $(post).siblings().hide();
                $(post).fadeIn();
            });

            $('#nadpis').val('').focus();
            $('#text').val('');

            var addpostform = $('.add-post-form');
            $('.add-post-form').remove();
            $('.posts-div').append(addpostform);
        });

});
