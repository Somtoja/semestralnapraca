<?php
require "../dbconnection.php";

if (isset($_POST['signup-submit'])) {

    $username = $_POST['username'];
    $password = $_POST['password'];
    $password2 = $_POST['password2'];

    if (empty($username) || empty($password) || empty($password2)) {
        header("location: ../reg.php?error=emptyfields&username=".$username);
        exit();
    }
    elseif (!preg_match("/^[a-zA-Z0-9]*$/", $username)) {
        header("location: ../reg.php?error=invalidusername");
        exit();
    }
    elseif($password !== $password2){
        header("location: ../reg.php?error=passcheck&username=".$username);
        exit();
    }
    else {

        $sql = "SELECT meno FROM users where meno=?";
        $stmt = mysqli_stmt_init($conn);
        if (!mysqli_stmt_prepare($stmt, $sql)) {
            header("location: ../reg.php?error=sqlerror");
            exit();
        } else {
            mysqli_stmt_bind_param($stmt, "s", $username);
            mysqli_stmt_execute($stmt);
            mysqli_stmt_store_result($stmt);
            $resultCheck = mysqli_stmt_num_rows($stmt);
            if ($resultCheck > 0) {
                header("location: ../reg.php?error=usertaken");
                exit();
                } else {
                $sql = "INSERT INTO users (meno,heslo) VALUES (?,?)";
                $stmt = mysqli_stmt_init($conn);
                if (!mysqli_stmt_prepare($stmt, $sql)) {
                    header("location: ../reg.php?error=sqlerror");
                    exit();
                }
                else{
                    $hashedpassword = password_hash($password, PASSWORD_DEFAULT);
                    mysqli_stmt_bind_param($stmt, "ss",$username,$hashedpassword);
                    mysqli_stmt_execute($stmt);
                    header("location: ../reg.php?error=success");
                    exit();
                }
            }
        }
    }
    mysqli_stmt_close($stmt);
    mysqli_close($conn);
}
else {
    header("location: ../reg.php");
    exit();
}