<!DOCTYPE html>
<html lang="sk">
<head>
	<meta charset="utf-8">
	<title>VAII / Blog</title>
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link rel="stylesheet" href="ext/style.css">
</head>
<body class="blog">

	<?php
		require "dbconnection.php";
		$query = "SELECT nadpis, text FROM posts";
	?>

    <?php
    require 'header.php';
    ?>

	<main>
		<section class="content container-bigger">
			<h1 class="shadow">Múdre myšlienky</h1>

			<h2 class="shadow">
				Nie som malý, len žijem vo veľkom svete!
			</h2>
			<div class="row">
				<div class="sidebar-div">
					<aside class="sidebar">

                        <?php
                        $result = mysqli_query($conn, $query);
                        if (mysqli_num_rows($result) > 0) {
                            while($post = mysqli_fetch_assoc($result)) {
                                echo '<div class="post-preview">';
                                echo '<p>' .  $post["nadpis"] . '</p>';
                                echo '<a href="edit.php><i class="fas fa-edit"></i></a>';
                                echo '</div>';
                            }
                        }
                        ?>

					</aside>
                    <?php
                    if (isset($_SESSION['username'])){
                       echo '<a class="btn btn-yellow btn-add-post">Pridať nový príspevok</a>';
                    }
                    ?>

				</div>

				<div class="posts-div">

                    <?php
                    $result = mysqli_query($conn, $query);
                    if (mysqli_num_rows($result) > 0) {
                        while($post = mysqli_fetch_assoc($result)) {
                            echo '<article class="post">';
                            echo '<h2 class="post-title shadow">' . $post["nadpis"] . '</h2>';
                            echo '<div class="post-text shadow">';
                            echo '<p>' . $post["text"] . '</p>';
                            echo '</div>';
                            if (isset($_SESSION['username'])){ echo '<a href=""><i class="trash-icon fas fa-2x fa-trash-alt"></i></a>'; }
                            echo '</article>';
                        }
                    }
                    ?>

                    <article class="post add-post-form">
                        <h2 class="post-title">Pridať nový príspevok</h2>
                        <div class="post-text">
                            <form class="post-form" method="POST" action="./phps/add-post.php">
                                <label for="nadpis">
                                    Nadpis
                                    <input type="text" id="nadpis" name="nadpis" required>
                                </label>
                                <label for="text">
                                    Text
                                    <textarea id="text" name="text" cols="40" rows="10" required></textarea>
                                </label>
                                <div class="btn" id="post-submit-btn">Pridať</div>
                            </form>
                        </div>
                    </article>
                </div>
			</div>
        </section>
	</main>

	<aside class="pre-footer">
		<div class="container">
			<h3>Footer</h3>

            <ul>
                <li class="green"><a href="index.php">Index</a></li>
                <li class="yellow"><a href="galeria.php">Galéria</a></li>
                <li class="red"><a href="kontakt.php">Kontakt</a></li>
                <li class="blue"><a href="blog.php">Blog</a></li>
            </ul>

            <p>Predmet : 5US109 vývoj aplikácií pre internet a intranet<br>
                Radovan Žiak<br>
                5ZY038</p>

			<a href="#" class="btn btn-green">Scroll up</a>
		</div>
	</aside>


    <?php
    require 'footer.php';
    ?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="./ext/app.js"></script>

</body>
</html>
