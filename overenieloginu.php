<?php

if(isset($_POST['login'])) {

    require "dbconnection.php";

    $username = $_POST['meno'];
    $password = $_POST['heslo'];

    if (empty($username) || empty($password)){
        header("Location: login.php?error=emptyfields");
        exit();
    }
    else {
       $sql = "SELECT * from users where meno=?;";
       $stmt = mysqli_stmt_init($conn);
       if (!mysqli_stmt_prepare($stmt, $sql)){
           header("Location: login.php?error=sqlerror");
           exit();
       }
       else {
           mysqli_stmt_bind_param($stmt, "s", $username);
           mysqli_stmt_execute($stmt);
           $result = mysqli_stmt_get_result($stmt);
           if ($row = mysqli_fetch_assoc($result)) {
               $pwdCheck = password_verify($password, $row['heslo']);
               if ($pwdCheck == false){
                   header("Location: login.php?error=wrongpassword");
                   exit();
               }
               elseif($pwdCheck == true){
                   session_start();
                   $_SESSION['userID'] = $row['id_user'];
                   $_SESSION['username'] = $row['meno'];

                   header("Location: login.php?error=success");
                   exit();
               }
               else {
                   header("Location: login.php?error=wrongpassword");
                   exit();
               }
           }
           else{
               header("Location: login.php?error=nouser");
               exit();
           }
       }
    }
}
else {
    header("Location: login.php");
    exit();
}