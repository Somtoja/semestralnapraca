<!DOCTYPE html>
<html lang="sk">
<head>
	<meta charset="utf-8">
	<title>VAII / Kontakt</title>
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
	<link rel="stylesheet" href="ext/style.css">
</head>
<body class="contact">

<?php
require 'header.php';
?>

	<main>
		<div class="content container">
			<h1 class="shadow">Napíš mi správu</h1>

			<h2 class="shadow">
				Aj tak si ju neprečítam!
			</h2>

			<form class="contact-form" method="POST" action="./phps/sprava.php">
				<label for="meno">
					Meno
					<input type="text" id="meno" name="meno" required>
				</label>

				<label for="email">
					E-mail
					<input type="email" id="email" name="email" placeholder="@" required>
				</label>

				<label for="sprava">
					Správa
					<textarea id="sprava" name="sprava" cols="40" rows="10" required></textarea>
				</label>

				<button class="btn btn-white" id="submit-btn">Odoslať</button>
			</form>
		</div>
	</main>

	<aside class="pre-footer">
		<div class="container">
			<h3>Footer</h3>

			<ul>
				<li class="green"><a href="index.php">Index</a></li>
				<li class="yellow"><a href="galeria.php">Galéria</a></li>
				<li class="red"><a href="kontakt.php">Kontakt</a></li>
				<li class="blue"><a href="blog.php">Blog</a></li>
			</ul>

			<p>Predmet : 5US109 vývoj aplikácií pre internet a intranet<br>
				Radovan Žiak<br>
				5ZY038</p>

			<a href="#" class="btn btn-green">Scroll up</a>
		</div>
	</aside>

<?php
require 'footer.php';
?>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="./ext/app.js"></script>

</body>
</html>
