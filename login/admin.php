<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <title>VAII / Index</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="../ext/style.css">
    <script src="../ext/jq.js"></script>
    <script src="../ext/app.js"></script>
</head>
<body id="secretroom" class="home">

<?php
require '../header.php';

if (isset($_SESSION['username'])) {

  echo  '<main >
        <section class="content container" >
            <h1 > Stránka po prihlásení </h1 >
            <h2 class="shadow" > Secret room </h2 >

            <div class="image-grid group" >
                <iframe width = "420" height = "315"
                        src = "https://www.youtube.com/embed/YlUKcNNmywk" >
                </iframe >
                <iframe width = "420" height = "315"
                        src = "https://www.youtube.com/embed/kPBzTxZQG5Q" >
                </iframe >
                <iframe width = "420" height = "315"
                        src = "https://www.youtube.com/embed/gEPmA3USJdI" >
                </iframe >
                <iframe width = "420" height = "315"
                        src = "https://www.youtube.com/embed/YR5ApYxkU-U" >
                </iframe >
                <iframe width = "420" height = "315"
                        src = "https://www.youtube.com/embed/1w7OgIMMRc4" >
                </iframe >
                <iframe width = "420" height = "315"
                        src = "https://www.youtube.com/embed/lDK9QqIzhwk" >
                </iframe >
            </div >
        </section >
    </main >


<aside class="pre-footer" >
    <div class="container" >
        <h3 > Footer</h3 >

        <ul >
            <li class="green" ><a href = "../index.php" > Index</a ></li >
            <li class="yellow" ><a href = "../galeria.php" > Galéria</a ></li >
            <li class="red" ><a href = "../kontakt.php" > Kontakt</a ></li >
            <li class="blue" ><a href = "../blog.php" > Blog</a ></li >
        </ul >

        <p > Predmet : 5US109 vývoj aplikácií pre internet a intranet < br>
            Radovan Žiak < br>
            5ZY038 </p >

    </div >
</aside >';

require '../footer.php';
}
elseif (!isset($_SESSION['username'])){
    echo '<p> <h1> Najprv sa prihlas !</h1> </p>';
}
?>

</body>
</html>