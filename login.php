<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <title>VAII / Index</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="ext/style.css">
    <script src="ext/jq.js"></script>
    <script src="ext/app.js"></script>
</head>
<body class="home">

<?php
require 'dbconnection.php';
$query = "SELECT meno FROM users";
?>

<?php
require 'header.php';
?>

<main>
    <div class="container">
        <div class="login">
            <div class="login-right">
                <form action="overenieloginu.php" id="loginform" method="post">
                    <h4> Prihlasenie </h4>
                    <?php
                    if(isset($_GET['error'])){
                        if ($_GET['error'] == "emptyfields") {
                            echo '<script>alert("Zadajte vsetky udaje!")</script>';
                        }
                        else if ($_GET['error'] == "wrongpassword"){
                            echo '<script>alert("Zadali ste nespravne heslo!")</script>';
                        }
                        else if ($_GET['error'] == "nouser"){
                            echo '<script>alert("Pouzivatel neexistuje")</script>';
                        }
                        else if ($_GET['error'] == "success"){
                            header("Location: http://localhost/SemestralnaPraca/index.php");
                        }

                    }
                    ?>
                    <input type="text" placeholder="Username" name="meno" id="meno">
                    <input type="password" placeholder="Password" name="heslo" id="heslo">
                    <input type="submit" id="login" name="login" value="Prihlasit!">
                </form>
            </div>
    </div>
    </div>
</main>

<?php
require 'footer.php';
?>

</body>
</html>
