<!DOCTYPE html>
<html lang="sk">
<head>
    <meta charset="utf-8">
    <title>VAII / Index</title>
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Montserrat:400,700">
    <link rel="stylesheet" href="ext/style.css">
    <script src="ext/jq.js"></script>
    <script src="ext/app.js"></script>
</head>
<body class="home">


<?php
require 'header.php';
?>

<main>
    <div class="container">
        <div class="login">

            <div class="login-right">
                <form id="loginform" method="post" action="registracia/connect.php" >
                    <h4>Registracia</h4>
                    <?php
                    if(isset($_GET['error'])){
                    if ($_GET['error'] == "emptyfields") {
                    echo '<script>alert("Zadajte vsetky udaje!")</script>';
                    }
                    else if ($_GET['error'] == "invalidusername"){
                    echo '<script>alert("Nespravne meno!")</script>';
                    }
                    else if ($_GET['error'] == "passcheck"){
                    echo '<script>alert("Hesla sa nezhoduju")</script>';
                    }
                    else if ($_GET['error'] == "usertaken"){
                    echo '<script>alert("Pouzivatel uz existuje")</script>';
                    }
                    else if ($_GET['error'] == "success"){
                        echo '<script>alert("Uspesna registracia")</script>';
                    }
                    }
                    ?>
                    <input type="text" placeholder="Username" name="username" id="meno">
                    <input type="password" placeholder="Password" name="password" id="heslo">
                    <input type="password" placeholder="Repeat password" name="password2" id="heslo2">
                    <input type="submit" name="signup-submit" id="registration" value="Registrovat!">
                </form>
            </div>
        </div>
    </div>
</main>

<?php
require 'footer.php';
?>

</body>
</html>
